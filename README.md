# Udemy
![UDEMY Courses](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F81ca3b54-bb75-458f-bd81-3eab7343567b_1200x630.png)

## Sobre o Projeto

**Missão: “Melhorar a vida das pessoas através do aprendizado”.**

**Visão: “Um mundo onde quem quer aprender vai sempre encontrar um instrutor certo para ensinar”.**

**Valores: “LET’s GO: Learn, Empathize, Take Ownership, Innovate, Show Passion, Get Stuff Done and Open Up (Aprender, Simpatizar, Assumir Controle, Inovar, Demonstrar paixão, entregar resultados e ter uma mente aberta)”.**

## Arquivos do Projeto

- Para acessar o Cronograma [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/CRONOGRAMA.docx)

- Para acessar os Integrantes + Tema [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/INTEGRANTES_+_TEMA.docx)

- Para acessar o MVP  [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/MVP/MVP.pptx)

- Para acessar o Mapa de Conhecimento [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/STEAM-GAMES.xmind)

- Para acessar a Matriz de Habilidade Inicial [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/MATRIZ_DE_HABILIDADES_INICIAL.xlsx)

- Para acessar a Matriz de Habilidade Final [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/MATRIZ_DE_HABILIDADES_FINAL.xlsx)

- Para acessar as Lições Aprendidas - Marcelo [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/Licoes_-_Marcelo.docx)

- Para acessar o Estudo sobre Grafos  - Marcelo [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/Grafos/Grafos_-_Marcelo.docx)

- Para acessar o Modelo de Dados [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/MODELO-DE-DADOS.xmind)

- Para acessar o Project Charter [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/PROJECT_CHARTER.pptx)

- Para acessar a Apresentação [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/STEAM-GAMES.pptx)

- Para acessar o Roteiro de Mineração de dados [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/Steam-games.txt)

## Pastas do Projeto

- Para acessar os Currículos [clique aqui.](https://gitlab.com/BDAg/udemy/-/tree/master/Curr%C3%ADculos)

- Para acessar os Estudos [clique aqui.](https://gitlab.com/BDAg/udemy/-/tree/master/Estudos)

- Para acessar os Grafos  [clique aqui.](https://gitlab.com/BDAg/udemy/-/tree/master/Grafos)

- Para acessar o MVP [clique aqui.](https://gitlab.com/BDAg/sudemy/-/tree/master/MVP)

- Para acessar os Scripts [clique aqui.](https://gitlab.com/BDAg/udemy/-/tree/master/Python%20Scripts)


## Programas Utilizados
- Para acessar o XMind  [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/XMind-2020-for-Windows-64bit-10.3.1-202101070032.exe)

- Para acessar o NoSqlBoosterMongo [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/nosqlbooster4mongo-5.2.10.exe)

- Para acessar o Mapa de LightShot [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/setup-lightshot.exe)

- Para acessar a DezignForDatabases [clique aqui.](https://gitlab.com/BDAg/udemy/-/blob/master/setup_dezign.exe)

## Andamento do Projeto

- [X] Entrega 01
- [X] Entrega 02
- [X] Entrega 03
- [X] Entrega 04
- [X] Sprint 1
- [X] Sprint 2
- [X] Sprint 3
- [X] Fechamento do Projeto

## Time

- Marcelo Augusto [[Perfil.]](https://gitlab.com/marcelinhoshow1)

## Professor

- Mauricio Duarte [[Perfil.]](https://gitlab.com/BDAg/steam-games)
- João Ricardo [[Perfil.]](https://gitlab.com/BDAg/steam-games)


